//TASK 1
function arrayMax(array) {
  return array.reduce((acc, elem) => Math.max(acc, elem));
}

//TASK 2
let a = 3;
let b = 5;

[a, b] = [b, a];

console.log(a);
console.log(b);

//TASK 3
function returnDefinedValue(val) {
  return val ?? '-';
}

//TASK 4
function arrayOfArrays(arr) {
  return Object.fromEntries(arr);
}

// function arrayOfArrays(arr) {
//   let obj = {};

//   arr.forEach(function (item) {
//     obj[item[0]] = item[1];
//   });

//   return obj;
// }

//TASK 5
function addUniqueId(obj) {
  return { id: Symbol(), ...obj };
}

//TASK 6
function getRegroupedObject({
  name: firstName,
  details: { id, age, university }
} = Object) {
  return { university, user: { age, firstName, id } };
}

//TASK 7
function getArrayWithUniqueElements(arr) {
  let arrSet = new Set();

  arr.forEach((element) => {
    arrSet.add(element);
  });
  return arrSet;
}

//TASK 8
function hideNumber(number) {
  return number.slice(-4).padStart(number.length - 1, '*');
}

//TASK 9
const isRequired = (par) => {
  throw new Error(par + ' is required');
};

function add(a = isRequired('a'), b = isRequired('b')) {
  return a + b;
}

//TASK 10
function* generateIterableSequence() {
  yield 'I';
  yield 'Love';
  yield 'Epam';
  return;
}

const generatorObject = generateIterableSequence();

for (let value of generatorObject) {
  console.log(value);
}
