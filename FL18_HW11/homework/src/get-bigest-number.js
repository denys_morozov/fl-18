const evenNumbersInArray = (array) => {
    if (!Array.isArray(array) || array.length === 0) {
 return 'Passed argument is not an array or empty'; 
}
    let ar = array.filter((number) => number % 2 === 0);
    return ar.length > 0 ? ar : 'Passed array does not contain even numbers';
  };
  
  module.exports = evenNumbersInArray;