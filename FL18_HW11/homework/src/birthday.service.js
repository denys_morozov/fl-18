// Your code goes here
const fibonacciNumbers = (num) => {
    if (typeof num !== 'number') {
 return 'Passed argument is not a number'; 
}
    if (num <= 1) {
 return 1; 
}
  
    let n1 = 0,
      n2 = 1,
      nextTerm;
  
    for (let i = 1; i <= num - 1; i++) {
      nextTerm = n1 + n2;
      n1 = n2;
      n2 = nextTerm;
    }
    return nextTerm;
  };
  module.exports = fibonacciNumbers;