class Magazine {
  constructor() {
    this.statesList = {
      ReadyForPushNotification: 'ReadyForPushNotification',
      ReadyForApprove: 'ReadyForApprove',
      ReadyForPublish: 'ReadyForPublish',
      PublishInProgress: 'PublishInProgress'
    };
    this.state = this.statesList.ReadyForPushNotification;
    this.staff;
    this.topics = ['sport', 'politics', 'general'];
    this.articles_Published = [];
    this.articles_AwaitingPublishing = [];
    // {
    //     role: "sport",
    //     article: "something about sport"
    // }
    this.isApproved;
    this.followers = [];
    // {
    //     user: name,
    //     subscriptions: ["sport, "general"]
    // }
    this.num_ArticlesForApprove = 5;
  }

  onNewSubscription(name, topic, user) {
    let isDone = false;

    //check if user already exists
    this.followers.forEach((follower) => {
      if (follower.user === name) {
        // console.log("There is such user as " + name);

        //check if user already subscribed
        ////CHANGE TO "IF CONTAINS"
        follower.subscriptions.forEach((sub) => {
          isDone = true;
          if (sub === topic) {
            console.log('The user is already subscribed to this topic '); 
}else {
            follower.subscriptions.push(topic);
            console.log(`The user ${name} subscribed to ${topic} topic`);
          }
        });
      }
    });

    // stop function if already added topic
    if (isDone) {
      return;
    }

    //OTHERWISE

    // push NEW user with NEW subscription
    this.followers.push({
      name: name,
      subscriptions: [topic],
      link: user
    });
    console.log(`A NEW user ${name} subscribed to ${topic} topic`);
    return;
  }

  onUnsubscription(name, topic) {
    // find the user
    this.followers.forEach((follower) => {
      if (follower.user === name) {
        console.log(
          'There is such user as ' +
            name +
            ' who wants to unsubscribe from ' +
            topic
        );

        for (let i = 0; i < follower.subscriptions.length; i++) {
          if (follower.subscriptions[i] === topic) {
            follower.subscriptions.splice(i, 1);
          }
        }
      }
    });
  }

  approve(name, role) {
    // check if user has rights for approval
    if (role !== 'manager') {
      console.log(`${name} you do not have permissions to do it`);
      return false;
    }

    switch (this.state) {
      case this.statesList.ReadyForPushNotification:
        console.log(
          `Hello ${name}.  You can't approve. We don't have enough of publications.`
        );
        return false;
      case this.statesList.ReadyForPublish:
        console.log(
          `Hello ${name} Publications have been already approved by you.`
        );
        return false;
      case this.statesList.PublishInProgress:
        console.log(
          `Hello ${name}.  While we are publishing we can't do any actions`
        );
        return false;
      // State: “ReadyForApprove”
      default:
        // check if there is enough articles for publishing
        if (
          this.articles_AwaitingPublishing.length !==
          this.num_ArticlesForApprove
        ) {
          console.log(
            `Hello ${name}.  You can't approve. We don't have enough of publications.`
          );
          return false;
        }

        // Approve
        this.isApproved = true;
        this.changeState(this.statesList.ReadyForPublish);
        console.log(`Hello ${name} You've approved the changes`);
        return true;
    }
  }

  publish(name) {
    switch (this.state) {
      case this.statesList.ReadyForPushNotification:
        console.log(
          `Hello ${name}.  You can't publish. We are creating publications now.`
        );
        return false;
      case this.statesList.ReadyForApprove:
        console.log(
          `Hello ${name} You can't publish. We don't have a manager's approval.`
        );
        return false;
      case this.statesList.PublishInProgress:
        console.log(
          `Hello ${name}.  While we are publishing we can't do any actions.`
        );
        return false;
      // State: "ReadyForPublish"
      default:
        // Publish
        console.log(`Hello ${name} You've recently published publications.`);
        this.changeState(this.statesList.PublishInProgress);

        setTimeout(() => {
          //transfer articles to the list of already published
          this.updateFollowers();
          this.articles_Published.push(...this.articles_AwaitingPublishing);
          this.articles_Published = [];
          this.changeState(this.statesList.ReadyForPushNotification);
          return true;
        }, 6000);
    }
  }

  updateFollowers() {
    this.followers.forEach((follower) => {
      this.articles_AwaitingPublishing.forEach((article) => {
        if (follower.subscriptions.includes(article.topic)) {
          follower.link.onUpdate(article.article);
        }
      });
    });
  }

  addArticle(name, role, article) {
    switch (this.state) {
      case this.statesList.ReadyForPublish:
        console.log(
          `Hello ${name} You can't add Article. We are waiting for publication`
        );
        return false;
      case this.statesList.PublishInProgress:
        console.log(
          `Hello ${name}.  While we are publishing we can't do any actions`
        );
        return false;
      // State: “ReadyForPushNotification” or “ReadyForApprove”
      default:
        // Check if user can add articles
        if (!this.topics.includes(role)) {
          console.log(`Hello ${name}.  You can't add articles. `);
        }

        // Add the article
        this.articles_AwaitingPublishing.push({
          topic: role,
          article: article
        });
        console.log(`Hello ${name} You've added a new article`);
        console.log(
          `*Current number of articles: ${this.articles_AwaitingPublishing.length}`
        );
        if (
          this.articles_AwaitingPublishing.length ===
          this.num_ArticlesForApprove
        ) {
          console.log(`*The articles are ready for approval`);
        }

        if (
          this.articles_AwaitingPublishing.length ===
          this.num_ArticlesForApprove
        ) {
          this.changeState(this.statesList.ReadyForApprove);
        }
        return true;
    }
  }

  changeState(state) {
    this.state = state;
  }
}

class MagazineEmployee {
  constructor(name, role, magazine) {
    this.name = name;
    this.role = role; //[]
    this.magazine = magazine;
  }

  addArticle(title) {
    magazine.addArticle(this.name, this.role, title);
  }

  approve() {
    magazine.approve(this.name, this.role);
  }

  publish() {
    magazine.publish(this.name);
  }

  onUpdate(data) {
    console.log(data + ' ' + this.name);
  }
}

class Follower {
  constructor(name) {
    this.name = name;
    this.subscriptions = [];
  }

  subscribeTo(magazine, topic) {
    let isSubscribed;
    this.subscriptions.forEach((sub) => {
      //   console.log(sub.topic);
      if (sub.topic === topic) {
        console.log(this.name + `: already subscribed to ${topic} topic`);
        isSubscribed = true;
      }
    });

    if (isSubscribed) {
      return;
    }

    this.subscriptions.push({
      magazine: magazine,
      topic: topic
    });

    magazine.onNewSubscription(this.name, topic, this);
  }

  unsubscribeFrom(magazine, topic) {
    console.log(this.name + `: I want to unsubscribe from ${topic} topic`);
    for (let i = 0; i < this.subscriptions.length; i++) {
      if (this.subscriptions[i].topic === topic) {
        this.subscriptions.splice(i, 1);
      }
    }

    magazine.onUnsubscription(this.name, topic);
  }

  onUpdate(data) {
    console.log(data + ' ' + this.name);
  }
}

//////////////////////////////////////////////////////

// const magazine = new Magazine();

// const manager = new MagazineEmployee('Andrii', 'manager', magazine);
// const sport = new MagazineEmployee('Serhii', 'sport', magazine);
// const politics = new MagazineEmployee('Volodymyr', 'politics', magazine);
// const general = new MagazineEmployee('Olha', 'general', magazine);

// const iryna = new Follower('Iryna');
// iryna.subscribeTo(magazine, 'sport');
// iryna.subscribeTo(magazine, 'sport');
// iryna.subscribeTo(magazine, 'general');
// iryna.unsubscribeFrom(magazine, 'sport');

// const maksym = new Follower('Maksym');
// maksym.subscribeTo(magazine, 'sport');
// maksym.subscribeTo(magazine, 'politics');
// maksym.subscribeTo(magazine, 'general');

// sport.addArticle('something about sport');
// politics.addArticle('something about politics');
// general.addArticle('some general information');
// politics.addArticle('something about politics again');
// sport.approve(); //you do not have permissions to do it
// manager.approve(); //Hello Andrii. You can't approve. We don't have enough of publications
// politics.publish(); //Hello Volodymyr. You can't publish. We are creating publications now.
// sport.addArticle('news about sport');
// manager.approve(); //Hello Andrii. You've approved the changes
// sport.publish(); //Hello Serhii. You've recently published publications.

// manager.approve("news about sport"); //Hello Andrii. While we are publishing we can't do any actions
