const rules = () => {
  const startBtn = document.querySelector(".startgame");
  const landingPage = document.querySelector(".rules");

  startBtn.addEventListener("click", () => {
    landingPage.remove();
  });
};

export { rules };
