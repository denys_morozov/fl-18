const restart = () => {
  const reloadBtn = document.querySelector(".reload");
  reloadBtn.addEventListener("click", () => {
    window.location.reload();
  });
};

export { restart };
