import { game } from "./js/game.js";
import { restart } from "./js/restart.js";
import { rules } from "./js/rules.js";

import "./styles/main.scss";
import "./styles/media.scss";

// Calling the game function
game();
restart();
rules();
