const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
// const CssnanoPlugin = require('cssnano-webpack-plugin');
const TersetrPlugin = require("terser-webpack-plugin");

const PrettierPlugin = require("prettier-webpack-plugin");
const ImageminPlugin = require("imagemin-webpack");

module.exports = {
  mode: "production",
  optimization: {
    minimizer: [new OptimizeCssAssetsPlugin(), new TersetrPlugin()],
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: {
          loader: "html-loader",
          options: { minimize: true },
        },
      },
      {
        test: /\.scss$/,
        // use: ["style-loader", "css-loader", "sass-loader"],
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html",
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      // chunkFilename: "[id].css",
    }),
    new PrettierPlugin({
      printWidth: 80, // Specify the length of line that the printer will wrap on.
      tabWidth: 2, // Specify the number of spaces per indentation-level.
      useTabs: false, // Indent lines with tabs instead of spaces.
      semi: true, // Print semicolons at the ends of statements.
      encoding: "utf-8", // Which encoding scheme to use on files
      extensions: [
        ".css",
        ".graphql",
        ".js",
        ".json",
        ".jsx",
        ".less",
        ".sass",
        ".scss",
        ".ts",
        ".tsx",
        ".vue",
        ".yaml",
      ],
    }),
  ],
};
