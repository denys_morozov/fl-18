const WebSocket = require('ws');

const webSocketServer = new WebSocket.Server({ port: 8080 });

webSocketServer.on('connection', (webSocket) => {
  webSocket.on('message', (data) => {
    console.log('Received message:', data);
    broadcast(data);
  });
});

function broadcast(data) {
  webSocketServer.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      console.log('broadcast:', data);
      client.send(data);
    }
  });
}
