const connection = new WebSocket('ws://localhost:8080');
let username;

connection.onopen = () => {
  console.log('connected');

  username = prompt('Enter your username:') || 'Anonymous';
};

connection.onclose = () => {
  console.error('disconnected');
};

connection.onerror = (error) => {
  console.error('failed to connect', error);
};

connection.onmessage = (event) => {
  console.log('received', event.data);
  let data = JSON.parse(event.data);
  console.log('json data:', data);
  let li = document.createElement('li');
  li.innerHTML = `
  <h6>${data.username}</h6>
  <p class="message_content"></p>
  <p>${getDateTime()}</p>`; //event.data   ${event.data.message}
  li.querySelector('.message_content').innerText = data.message;
  document.querySelector('#chat').append(li);

  if (data.username === username) {
    li.classList += 'message__own';
  } else {
    li.classList += 'message__friend';
  }
};

document.querySelector('form').addEventListener('submit', (event) => {
  event.preventDefault();
  let message = document.querySelector('#message').value;
  console.log(message);

  connection.send(
    JSON.stringify({
      username: username,
      message: message
    })
  );
  document.querySelector('#message').value = '';
});

function getDateTime() {
  let now = new Date();
  let year = now.getFullYear();
  let month = now.getMonth() + 1;
  let day = now.getDate();
  let hour = now.getHours();
  let minute = now.getMinutes();
  let second = now.getSeconds();
  if (month.toString().length === 1) {
    month = '0' + month;
  }
  if (day.toString().length === 1) {
    day = '0' + day;
  }
  if (hour.toString().length === 1) {
    hour = '0' + hour;
  }
  if (minute.toString().length === 1) {
    minute = '0' + minute;
  }
  if (second.toString().length === 1) {
    second = '0' + second;
  }
  let dateTime =
    year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
  return dateTime;
}
