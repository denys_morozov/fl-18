// Your code goes here

let jsonFile;

let usersContainer = document.querySelector('.users-container');
let editForm = document.querySelector('#editForm');
let user_id = editForm.querySelector('#id');
let user_name = editForm.querySelector('#name');
let user_company = editForm.querySelector('#company');
let user_email = editForm.querySelector('#email');

let userItem;
let userItem_id;
let userItem_name;
let userItem_company;
let userItem_email;

createUsersList();

function createUsersList() {
  fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => response.json())
    .then((json) => {
      console.log(json);
      jsonFile = json;

      json.forEach(function (item) {
        let itemText = ` 
<div class="item">
<ul>
<li>ID: <span id="userId">${item.id}</span></li>
<li>Name: <span id="userName">${item.name}</span></li>
<li>Company: <span id="companyName">${item.company.name}</dspaniv></li>
<li>email: <span id="userEmail">${item.email}</span></li>
</ul> 

<button id="editUser" onclick="editUser(event)" style="margin-left: 10px;">Edit</button>
<button onclick="deleteUser(event)">Delete</button>
</div>
    `;

        usersContainer.innerHTML += itemText;
      });
    });
}

function editUser(event) {
  userItem = event.target.parentElement;
  console.log(userItem);

  userItem_id = userItem.querySelector('#userId');
  userItem_name = userItem.querySelector('#userName');
  userItem_company = userItem.querySelector('#companyName');
  userItem_email = userItem.querySelector('#userEmail');

  console.log(userItem_name.innerText);

  user_id.value = userItem_id.innerText;
  user_name.value = userItem_name.innerText;
  user_company.value = userItem_company.innerText;
  user_email.value = userItem_email.innerText;
}

function updateUser(event) {
  event.preventDefault();

  console.log('user_id.value: ' + user_id.value);
  fetch(`https://jsonplaceholder.typicode.com/users/${user_id.value - 1}`, {
    method: 'PUT',
    body: JSON.stringify({
      id: user_id.value,
      name: user_name.value,
      username: jsonFile[user_id.value - 1].username,
      email: user_email.value,
      address: jsonFile[user_id.value - 1].address,
      geo: jsonFile[user_id.value - 1].geo,
      phone: jsonFile[user_id.value - 1].phone,
      website: jsonFile[user_id.value - 1].website,
      company: {
        name: user_company.value,
        catchPhrase: jsonFile[user_id.value - 1].company.catchPhrase,
        bs: jsonFile[user_id.value - 1].company.bs
      }
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
    .then((response) => response.json())
    .then((json) => {
      console.log(json);

      updateFields(json);
    });
}

function clearFields() {
  user_name.value = '';
  user_company.value = '';
  user_email.value = '';
}

function deleteUser(event) {
  let item = event.target.parentElement;
  fetch('https://jsonplaceholder.typicode.com/posts/id', {
    method: 'DELETE'
  });

  item.remove();
}

function updateFields(json) {
  userItem_name.innerText = json.name;
  userItem_company.innerText = json.company.name;
  userItem_email.innerText = json.email;
}
