// let gameBoard = document.querySelector(".game");
import { Players } from "./players.js";
import { Rules } from "./rules.js";
let players = new Players();
let rules = new Rules();

let cells = document.querySelectorAll(".cell");
let btn_clear = document.querySelector(".btn_clear");
let btn_newGame = document.querySelector(".btn_newGame");

let isVictory;

export function GameBoard() {
  this.fillCell = function (cell) {
    if (rules.hasSpace && !isVictory && cell.innerText === "") {
      cell.innerText = players.currentPlayer.symbol;
      players.switchPlayers();
      rules.updateMatrix(cells);
      if (rules.checkIfEnd()) {
        console.log("end");
      }
      if (players.onVictory(rules.checkIfVictory())) {
        console.log("win checkkk");
        isVictory = true;
      }

      if (!rules.hasSpace) {
        players.onDraw();
      }
    }
  };

  this.clearCells = function () {
    cells.forEach((cell) => {
      cell.innerText = "";
    });
    rules.resetMatrix();
  };

  cells.forEach((cell) => {
    cell.addEventListener("click", () => {
      this.fillCell(cell);
    });
  });

  btn_clear.addEventListener("click", () => {
    this.clearCells();
    isVictory = false;
    players.onRestart();
  });

  btn_newGame.addEventListener("click", () => {
    window.location.reload();
  });
}
