import { Players } from "./players.js";
import { GameBoard } from "./gameBoard.js";

let players = new Players();
let gameboard = new GameBoard();

export function Controls() {
  this.start = function () {
    players.updateScoreTable();
  };
}
