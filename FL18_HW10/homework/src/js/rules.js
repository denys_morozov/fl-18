export function Rules() {
  let matrix = [
    ["", "", ""],
    ["", "", ""],
    ["", "", ""],
  ];

  this.hasSpace = true;

  this.checkIfEnd = function () {
    for (let i = 0; i < 3; i++) {
      if (matrix[i].includes("")) {
        return false;
      }
    }

    this.hasSpace = false;
    console.log("end ");
  };

  this.updateMatrix = function (cells) {
    let index = 0;

    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = cells[index].innerText;
        index++;
      }
    }

    console.log(matrix);
  };

  this.resetMatrix = function () {
    matrix = [
      ["", "", ""],
      ["", "", ""],
      ["", "", ""],
    ];
    this.hasSpace = true;
    console.log("clear");
  };

  this.checkIfVictory = function () {
    for (let i = 0; i < 3; i++) {
      if (
        matrix[i][0] === matrix[i][1] &&
        matrix[i][1] === matrix[i][2] &&
        matrix[i][0] !== ""
      ) {
        // alert("victory");
        return matrix[i][0];
      } else if (
        matrix[0][i] === matrix[1][i] &&
        matrix[1][i] === matrix[2][i] &&
        matrix[0][i] !== ""
      ) {
        // alert("victory");
        return matrix[0][i];
      }
    }

    if (
      matrix[0][0] == matrix[1][1] &&
      matrix[1][1] == matrix[2][2] &&
      matrix[0][0] !== ""
    ) {
    //   alert("victory");
      return matrix[0][0];
    }

    if (
      matrix[0][2] == matrix[1][1] &&
      matrix[1][1] == matrix[2][0] &&
      matrix[0][2] !== ""
    ) {
    //   alert("victory");
      return matrix[0][2];
    }
  };
}
