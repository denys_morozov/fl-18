export function Players() {
  let result = document.querySelector(".result");

  this.player1 = {
    score: 0,
    ui: document.querySelector(".score .p1 span"),
    symbol: "O",
    name: "Player 1",
  };
  this.player2 = {
    score: 0,
    ui: document.querySelector(".score .p2 span"),
    symbol: "X",
    name: "Player 2",
  };
  this.currentPlayer = this.player2;

  this.updateScoreTable = function () {
    this.player1.ui.innerText = this.player1.score;
    this.player2.ui.innerText = this.player2.score;
  };

  this.increaseScore = function () {
    this.currentPlayer.score++;
    this.updateScoreTable();
  };

  this.switchPlayers = function () {
    this.currentPlayer =
      this.currentPlayer == this.player1 ? this.player2 : this.player1;
  };

  this.onVictory = function (symbol) {
    if (!symbol) return false;
    console.log("won: " + symbol);

    this.increaseScore();

    result.classList.toggle("result--shown");
    result.innerText = this.currentPlayer.name + " won!";
    return true;
  };

  this.onDraw = function () {
    result.classList.toggle("result--shown");
    result.innerText = "Draw!";
  };

  this.onRestart = function () {
    result.classList.toggle("result--shown");
  };
}

export function test() {
  this.start = function () {
    console.log("start");
  };
}
