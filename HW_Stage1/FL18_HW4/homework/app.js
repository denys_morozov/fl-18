function reverseNumber(num) {
    let digit,
      result = 0;
    let positive = Math.sign(num);
    num = Math.abs(num);
  
    while (num > 0) {
      digit = num % 10;
      result = result * 10 + digit;
      console.log('Math.floor(num / 10): ' + Math.floor(num / 10));
      num = parseInt(num / 10);
      console.log('numm: ' + num);
    }
  
    return result * positive;
  }
  
  function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
      func(arr[i], i, arr);
    }
  }
  
  function map(arr, func) {
    const mapArr = [];
    for (let i = 0; i < arr.length; i++) {
      const result = func(arr[i], i, arr);
      mapArr.push(result);
    }
    return mapArr;
  }
  
  function filter(arr, func) {
    const filterArr = [];
    for (let i = 0; i < arr.length; i++) {
      const result = func(arr[i], i, arr);
      if (result) {
        filterArr.push(arr[i]);
      }
    }
    return filterArr;
  }
  
  function getAdultAppleLovers(data) {
    const AppleLoversArr = [];
    for (let i = 0; i < data.length; i++) {
      const person = data[i];
      if (person['favoriteFruit'] === 'apple' && person['age'] >= 18) {
        AppleLoversArr.push(data[i]);
      }
    }
    return AppleLoversArr;
  }
  
  function getKeys(obj) {
    let keyArr = [];
    for (key in obj) {
      if (key) {
        keyArr.push(key);
      }
    }
    return keyArr;
  }
  
  function getValues(obj) {
    let keyArr = [];
    for (key in obj) {
      if (key) {
        keyArr.push(obj[key]);
      }
    }
    return keyArr;
  }
  
  function showFormattedDate(dateObj) {
    return `It is ${dateObj.toLocaleString('en-US', {
      day: 'numeric',
    })} of ${dateObj.toLocaleString('en-US', {
      month: 'short',
    })}, ${dateObj.toLocaleString('en-US', { year: 'numeric' })}`;
  }