
import {Players} from './players';
import {htmlToElement, addClassToElement, getCssVariable} from './functions';

const count_RowsColumns = getCssVariable('quantityRowsAndColumns');
let model = null;

export function createControls(modelArg) {
  model = modelArg;
  let controls = buildControls();

  let dataStore = {
    controls: controls,
    gameOver: false,
    players: new Players()
  };

  controls.addEventListener('click', event => cellCLicked(event, dataStore));

  let newGameEvent = () => restartGame(dataStore);
  model.eventBus.subscribe(model.eventBus.channelOptions.newGame, newGameEvent);

  return controls;
}

function restartGame(dataStore) {
  let SymbolsOnBoard = getAllCells(dataStore.controls, 'player-symbol');
  SymbolsOnBoard.forEach(symbol => symbol.remove());

  dataStore.gameOver = false;

  let redLines = getAllCells(dataStore.controls, 'red-line_horizontal');
  redLines.forEach(redLine => redLine.classList.remove('red-line_horizontal'));
  redLines = getAllCells(dataStore.controls, 'red-line_vertical');
  redLines.forEach(redLine => redLine.classList.remove('red-line_vertical'));
}

function buildControls() {
  const last__row_or_col = (count_RowsColumns - 1).toString();

  let controls = htmlToElement(`<section class="controls" aria-label="controls"></section>`);

  for (let row = 0; row < count_RowsColumns; row++) {
    let controls_row = htmlToElement(`<div class="controls__row"></div>`);

    for (let column = 0; column < count_RowsColumns; column++) {
      let cell = htmlToElement(`<div class="cell" data-col="${column}" data-row="${row}"></div>`);
      if(cell.dataset.col !== last__row_or_col) {
        addClassToElement(cell, 'cell_right-boarder');
      }
      if(cell.dataset.row !== last__row_or_col) {
        addClassToElement(cell, 'cell_bottom-border');
      }
      let redLine = htmlToElement(`<div class="red-line" data-row="${row}" data-col="${column}"></div>`);
      cell.appendChild(redLine);

      controls_row.appendChild(cell);
    }
    controls.appendChild(controls_row);
  }

  return controls;
}

function cellCLicked(event, dataStore) {
  let clickedCell = event.target;
  let { gameOver, players } = dataStore;

  if(!gameOver && isCell(clickedCell) && clickedCell.innerText === '') {
    clickedCell.innerHTML = `
      <span class="player-symbol">${players.getPlayerSymbol()}</span>`
      + clickedCell.innerHTML;

    let isWin = getIsWin(clickedCell, dataStore);
    if (isWin.status || getIsDraw(dataStore.controls)) {
      dataStore.gameOver = true;
      displayGameResult(isWin,dataStore);

    } else {
      players.setNextPlayerTurn();
    }
  }
}

function displayGameResult(isWin, dataStore) {
  let { players } = dataStore;

  if(isWin.status){
    if(players.getPlayerSymbol() === players.getPlayer1_Symbol()) {
      model.player1_score++;
      model.scoreboard_message = 'Player 1 won!';
    }else {
      model.player2_score++;
      model.scoreboard_message = 'Player 2 won!';
    }
  }else {
    model.player1_score++;
    model.player2_score++;
    model.scoreboard_message = 'Draw!';
  }

  model.eventBus.publish(model.eventBus.channelOptions.endGame);
}

function getIsDraw(controls) {
  let allCells = getAllCells(controls, 'cell');
  return allCells.every(cell => cell.innerText !== '');
}

function getAllCells(controls, className) {
  return Array.from(controls.querySelectorAll(`.${className}`));
}

function getIsWin(cell, dataStore) {
  let controls = dataStore.controls;

  if(isHorizontalSymbolsSame(cell, controls)){
    let redLines = getElementsInOneLine(controls, 'red-line', 'row', cell.dataset.row);
    redLines.forEach(line => addClassToElement(line, 'red-line_horizontal'));

    return {status: true, line: 'horizontal'};

  }else if(isVerticalSymbolsSame(cell, controls)) {
    let redLines = getElementsInOneLine(controls, 'red-line', 'col', cell.dataset.col);
    redLines.forEach(line => addClassToElement(line, 'red-line_vertical'));

    return {status: true, line: 'vertical'};

  }else if(isDiagnolSymbolsSameFromTopToBottom(controls)){
    return {status: true, line: 'TopToBottom'};
  }else if(isDiagnolSymbolsSameFromBottomToTop(controls)){
    return {status: true, line: 'BottomToTop'};
  }

  return {status: false, line: ''};
}

function getElementsInOneLine(controls, typeOfElement, colOrRowString, colOrRowNumber) {
  return Array.from(controls.querySelectorAll(`.${typeOfElement}[data-${colOrRowString}='${colOrRowNumber}']`));
}

function isVerticalSymbolsSame(cell, controls) {
  let verticalCells = Array.from(getElementsInOneLine(controls, 'cell', 'col', cell.dataset.col));
  return isCellsValueSame(verticalCells);
}

function isHorizontalSymbolsSame(cell, controls) {
  let horizontalCells = Array.from(getElementsInOneLine(controls, 'cell', 'row', cell.dataset.row));
  return isCellsValueSame(horizontalCells);
}

function isDiagnolSymbolsSameFromTopToBottom(controls) {
  let diagnolCellFromTopToBottom = [];
  for(let rowAndCol=0; rowAndCol < count_RowsColumns; rowAndCol++) {
    let cell = controls.querySelector(`[data-row='${rowAndCol}'][data-col='${rowAndCol}']`);
    diagnolCellFromTopToBottom.push(cell);
  }

  return isCellsValueSame(diagnolCellFromTopToBottom);
}

function isDiagnolSymbolsSameFromBottomToTop(controls) {
  let diagnolCellFromBottomToTop = [];
  for(let row=count_RowsColumns-1, col=0; row >= 0 && col < count_RowsColumns; row--, col++) {
    let cell = controls.querySelector(`[data-row='${row}'][data-col='${col}']`);
    diagnolCellFromBottomToTop.push(cell);
  }

  return isCellsValueSame(diagnolCellFromBottomToTop);
}

function isCellsValueSame(cells) {
  return cells.every(cell => cell.innerText === cells[0].innerText && cell.innerText !== '');
}

function isCell(target) {
  return target.className.includes('cell');
}