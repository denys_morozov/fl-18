/* START TASK 1: Your code goes here */

let table = document.querySelector('.table');
let cells = document.querySelectorAll('.cell');

table.addEventListener('click', function (e) {
  if (e.target && e.target.classList.contains('cell--special')) {
    cells.forEach((el) => {
      if (el.getAttribute('data-cellColor') === '') {
 el.style.backgroundColor = 'green'; 
}
      el.setAttribute('data-cellColor', 'green');
    });
  }

  let row = e.target.parentNode;
  console.log('parentNode: ' + e.target.parentNode);
  if (e.target === row.firstElementChild) {
    let children = row.getElementsByTagName('td');
    for (let i = 0; i < children.length; i++) {
      let cellColor = children[i].getAttribute('data-cellColor');
      if (cellColor !== 'green') {
        children[i].style.backgroundColor = 'blue';
        children[i].setAttribute('data-cellColor', 'blue');
      }
    }
  }

  if (
    e.target &&
    e.target.classList.contains('cell') &&
    e.target.getAttribute('data-cellColor') === ''
  ) {
    e.target.style.backgroundColor = 'yellow';
    e.target.setAttribute('data-cellColor', 'yellow');
  }
});

/* END TASK 1 */

/* START TASK 2: Your code goes here */

let tel__form = document.querySelector('.tel__form');
let tel__submit = document.querySelector('.tel__submit');
let tel__input = document.querySelector('.tel__input');
let tel__notification = document.querySelector('.tel__notification');
tel__notification.hidden = true;

tel__input.addEventListener('keyup', function (e) {
  let reg = /^\+380[0-9]{9}$/;
  tel__notification.textContent =
    'Type number does not follow format +380********';
  tel__input.classList.add('tel__input--fail');
  if (reg.test(e.target.value)) {
    console.log('valid change: ' + e.target.value);
    tel__submit.disabled = false;
    tel__notification.hidden = true;
  } else {
    console.log('invlaid change: ' + e.target.value);
    tel__submit.disabled = true;
    tel__notification.classList.remove('tel__notification--success');
    tel__notification.classList.add('tel__notification--fail');
    tel__notification.hidden = false;
  }
});

tel__form.addEventListener('submit', (e) => {
  e.preventDefault();
  tel__input.classList.remove('tel__input--fail');
  tel__notification.textContent = 'Data was successfully sent';
  tel__notification.hidden = false;
  tel__notification.classList.remove('tel__notification--fail');
  tel__notification.classList.add('tel__notification--success');
});

/* END TASK 2 */

/* START TASK 3: Your code goes here */

let court = document.querySelector('.court');
let ball = {
  element: document.querySelector('.ball'),
  left: window
    .getComputedStyle(document.querySelector('.ball'))
    .getPropertyValue('left'),
  top: window
    .getComputedStyle(document.querySelector('.ball'))
    .getPropertyValue('top'),
  isAvailable: true
};

let winZone = document.querySelectorAll('.win-zone');

let scoreboard = {
  teamA_score: 0,
  teamB_score: 0,
  teamA_span: document.querySelector('.scoreboard__teams-A'),
  teamB_span: document.querySelector('.scoreboard__teams-B'),
  scoreboardNotif: document.querySelector('.scoreboard__notif'),
  scoreboardNotif_text: (team) => `Team ${team} Score!`
};
scoreboard.scoreboardNotif.hidden = true;

winZone.forEach((element) => {
  element.addEventListener('click', function (e) {
    let goalEvent;

    if (e.target.getAttribute('data-team') === 'A') {
      scoreboard.teamA_score++;
      scoreboard.teamA_span.textContent = scoreboard.teamA_score;

      goalEvent = new CustomEvent('goal', {
        detail: {
          text: `Team A Score!`,
          color: 'red',
          timeout: 700
        }
      });
    } else {
      scoreboard.teamB_score++;
      scoreboard.teamB_span.textContent = scoreboard.teamB_score;

      goalEvent = new CustomEvent('goal', {
        detail: {
          text: `Team B Score!`,
          color: 'blue',
          timeout: 700
        }
      });
    }

    scoreboard.scoreboardNotif.dispatchEvent(goalEvent);
  });

  court.addEventListener('click', function (e) {
    if (ball.isAvailable) {
      let rect = court.getBoundingClientRect();
      let x = e.clientX - rect.left;
      let y = e.clientY - rect.top;
      console.log('Left? : ' + x + ' ; Top? : ' + y + '.');

      ball.element.classList.add('ball--move');
      ball.element.style.left = `${x}px`;
      ball.element.style.top = `${y}px`;

      ball.isAvailable = false;

      setTimeout(() => {
        ball.element.style.left = ball.left;
        ball.element.style.top = ball.top;
      }, 700);
      setTimeout(() => {
        ball.isAvailable = true;
      }, 1000);
    }
  });

  scoreboard.scoreboardNotif.addEventListener('goal', function (e) {
    scoreboard.scoreboardNotif.hidden = false;

    scoreboard.scoreboardNotif.textContent = e.detail.text;
    scoreboard.scoreboardNotif.style.color = e.detail.color;

    setTimeout(() => {
      scoreboard.scoreboardNotif.hidden = true;
    }, e.detail.timeout);
  });
});

/* END TASK 3 */
