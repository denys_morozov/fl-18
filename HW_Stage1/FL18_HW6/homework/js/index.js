function visitLink(path) {
  let count = parseInt(localStorage.getItem(path));

  count = isNaN(count) ? 1 : ++count;
  localStorage.setItem(path, count);
}

function viewResults() {
  if (!document.querySelector('#visited-links__list')) {
    let ul = document.createElement('ul');
    ul.id = 'visited-links__list';
    document.querySelector('#content').appendChild(ul);
  }

  for (let i = 0; i < localStorage.length; i++) {
    let li = document.createElement('li');
    let key = localStorage.key(i);
    let value = localStorage.getItem(key);
    li.innerHTML = `You visitedd ${key} ${value} time(s)`;
    document.querySelector('#visited-links__list').appendChild(li);
  }

  localStorage.clear();
}
