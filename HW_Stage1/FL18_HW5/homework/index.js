function isEquals(val1, val2) {
  return val1 === val2;
}

function isBigger(val1, val2) {
  return val1 > val2;
}

function storeNames(...args) {
  return args;
}

function getDifference(val1, val2) {
  return val1 > val2 ? val1 - val2 : val2 - val1;
}

function negativeCount(arr = []) {
  return arr.filter((el) => el < 0).length;
}

function letterCount(text, letter) {
  return text.split(letter).length - 1;
}

function countPoints(arr = []) {
  let totalPoints = 0;

  arr.forEach(function (item) {
    let points = item.split(':');

    if (Number(points[0]) > Number(points[1])) {
      totalPoints += 3;
    }
    if (Number(points[0]) === Number(points[1])) {
      totalPoints += 1;
    }
  });

  return totalPoints;
}
