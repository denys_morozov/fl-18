function getAge(date_Birth) {
  console.log('date_Birth: ' + date_Birth);
  let date_Today = new Date();
  let age = date_Today.getFullYear() - date_Birth.getFullYear();

  console.log('age: ' + age);

  let monthComparison = date_Today.getMonth() - date_Birth.getMonth();

  if (
    monthComparison < 0 ||
    monthComparison === 0 && date_Today.getDate() < date_Birth.getDate()
  ) {
    age--;
  }

  console.log('age2: ' + age);

  return age;
}

function getWeekDay(date) {
  let days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];

  return days[date.getDay()];
}

function getAmountDaysToNewYear(date) {
  let date_NewYear = new Date(date.getFullYear(), 11, 31);
  console.log('New Year is on: ' + date_NewYear);

  if (date.getMonth() === 11 && date.getDate() > 31) {
    date_NewYear.setFullYear(date_NewYear.getFullYear() + 1);
  }
  let one_day = 1000 * 60 * 60 * 24;

  return Math.ceil((date_NewYear.getTime() - date.getTime()) / one_day);
}

function getProgrammersDay(year) {
  const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  let oneDay = 60 * 60 * 24;
  let secondsBeforeHolliday = oneDay * 256;

  let d = new Date(year, 0, 0);
  d.setSeconds(secondsBeforeHolliday);

  return `${d.getDate()} ${monthNames[d.getMonth()]}, ${year} (${
    days[d.getDay()]
  })`;
}

function howFarIs(dayOfWeek) {
  const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];

  let today = new Date();

  let specifiedWeekday =
    dayOfWeek[0].toUpperCase() + dayOfWeek.slice(1).toLowerCase();

  let specifiedWeekday__dayOfWeek;

  specifiedWeekday__dayOfWeek = days.indexOf(specifiedWeekday);
  if (specifiedWeekday__dayOfWeek === -1) {
 return 'There is no such day'; 
}

  let daysBefore = specifiedWeekday__dayOfWeek - today.getDay();

  if (daysBefore > 0) {
    return `It's ${daysBefore} day(s) left till ${specifiedWeekday}`;
  } else if (daysBefore === 0) {
    return `Hey, today is ${specifiedWeekday} =)`;
  } else {
 return `Hey, ${specifiedWeekday} has passed ${Math.abs(
      daysBefore
    )} day(s) ago)`; 
}
}

function isValidIdentifier(parName) {
  let regex = /^[^0-9][a-zA-Z0-9_$]*$/;
  return regex.test(parName);
}

function capitalize(text) {
  let regex = /(\b[a-z](?!\s))/g;

  return text.replace(regex, function (char) {
    return char.toUpperCase();
  });
}

function isValidAudioFile(fileName) {
  let regex = /^[a-z]*\.(?:mp3|flac|alac|aac)$/i;
  return regex.test(fileName);
}

function isValidPassword(password) {
  let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
  return regex.test(password);
}

function addThousandsSeparators(text) {
  return text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function getAllUrlsFromText(text) {
  let urlArray = text.split(' ');
  let regex = /(http|ftp|https):\/\/(www\.)?(.*)?\/?(.)*/;

  return urlArray.filter((element) => regex.test(element));
}