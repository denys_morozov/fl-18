const appRoot = document.getElementById('app-root');

let btn_region = document.querySelector('#region');
let btn_language = document.querySelector('#language');
let select = document.querySelector('#search_query');

let table = document.querySelector('#table');
let table_body = document.querySelector('#table_body');
let note = document.querySelector('#note');

let list;

let prevSortBtn;

note.hidden = true;
table.hidden = true;

btn_region.addEventListener('click', function () {
  select.innerHTML = '';
  table.hidden = true;
  note.hidden = false;
  const placeholder = document.createElement('option');

  placeholder.textContent = 'Select value';
  placeholder.hidden = placeholder.selected = true;
  select.append(placeholder);

  for (const value of externalService.getRegionsList()) {
    const option = document.createElement('option');
    option.textContent = option.value = value;
    select.append(option);
  }
});

btn_language.addEventListener('click', function () {
  select.innerHTML = '';
  table.hidden = true;
  note.hidden = false;
  const placeholder = document.createElement('option');

  placeholder.textContent = 'Select value';
  placeholder.hidden = placeholder.selected = true;
  select.append(placeholder);

  for (const value of externalService.getLanguagesList()) {
    const option = document.createElement('option');
    option.textContent = option.value = value;
    select.append(option);
  }
});

select.addEventListener('change', () => {
  list = getCountryList();
  if (prevSortBtn !== undefined) {
    prevSortBtn.innerHTML = '&#8597;';
  }
  setupTable();
});

function getCountryList() {
  if (btn_region.checked) {
    return externalService.getCountryListByRegion(select.value);
  } else {
    return externalService.getCountryListByLanguage(select.value);
  }
}

function setupTable() {
  table.hidden = false;
  note.hidden = true;
  table_body.innerHTML = '';

  buildTable(list);
}

function buildTable(list) {
  list = list
    .map(
      ({ name, capital, region, languages, area }) => `<tr>
        <td>${name}</td>
        <td>${capital}</td>
        <td>${region}</td>
        <td>${Object.values(languages).join(', ')}</td>
        <td>${area}</td>
        <td><img src="#" alt='${name}-flag'></td>
    </tr>`
    )
    .join(''); // src=${flagURL} - error

  table_body.innerHTML += list;
}

function sortTable(event) {
  let target = event.target;
  let column = target.getAttribute('data-column');
  let order = target.getAttribute('data-order');
  console.log('column was clicked: ' + column + order);

  if (prevSortBtn !== undefined) {
    prevSortBtn.innerHTML = '&#8597;';
  }

  prevSortBtn = event.target;

  if (order === 'desc') {
    console.log('order: desc');
    target.innerHTML = '&darr;';
    event.target.setAttribute('data-order', 'asc');
    if (column === 'cName') {
      list = list.sort(function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
    } else {
      list = list.sort(function (a, b) {
        if (a.area > b.area) {
          return 1;
        }
        if (a.area < b.area) {
          return -1;
        }
        return 0;
      });
    }
  } else {
    console.log('order: asc');
    target.innerHTML = '&uarr;';
    event.target.setAttribute('data-order', 'desc');

    if (column === 'cName') {
      list = list.sort(function (a, b) {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
    } else {
      list = list.sort(function (a, b) {
        if (a.area > b.area) {
          return -1;
        }
        if (a.area < b.area) {
          return 1;
        }
        return 0;
      });
    }
  }

  setupTable();
}
