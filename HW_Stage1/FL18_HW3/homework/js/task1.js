let amount_initial;
let number_ofYears;
let percentage;

let amount_total;
let profit_total;

let infinitTrue = true;

const magicNumbers = {
  minAmountMoney: 1000,
  minNumberYear: 1,
  maxPercentage: 100,
  numOfFixed: 2
};

amount_initial = Number(
  getUserInput(
    'What is the initial amount of money?',
    (v) => v < magicNumbers.minAmountMoney
  )
);
number_ofYears = Number(
  getUserInput(
    'What is the number of years?',
    (v) => !Number.isInteger(+v) || v < magicNumbers.minNumberYear
  )
);
percentage = Number(
  getUserInput(
    'What is the percentage of a year?',
    (v) => v < 0 || v > magicNumbers.maxPercentage
  )
);

amount_total = countProfit();
profit_total = amount_total - amount_initial;

alert(
  'Initial amount: ' +
    amount_initial.toFixed(magicNumbers.numOfFixed) +
    ' \nNumber of years: ' +
    number_ofYears +
    ' \nPercentage of year: ' +
    percentage.toFixed(magicNumbers.numOfFixed) +
    ' \n \nTotal profit: ' +
    profit_total.toFixed(magicNumbers.numOfFixed) +
    '\nTotal amount: ' +
    amount_total.toFixed(magicNumbers.numOfFixed)
);

function getUserInput(text, selection) {
  do {
    let value = prompt(text);
    if (isNaN(value) || selection(value)) {
      alert('Invalid input data');
    } else {
      return value;
    }
  } while (infinitTrue);
}

function countProfit() {
  let result = amount_initial;
  console.log(result + ' start count');
  for (let i = number_ofYears; i > 0; i--) {
    result += result * percentage / magicNumbers.maxPercentage;
    console.log(result);
  }
  console.log(result + ' end count');
  return result;
}