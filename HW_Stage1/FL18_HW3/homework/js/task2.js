let gameInvitation_accepted;
let playerResponse;
let continueGame;
let infinitLoop = true;

const number = {
  maxValue: 8,
  startValue: 8,
  winNumber: 0
};

let successfulAttempt;
const attempts = {
  maxValue: 3,
  left: 3
};

const prize = {
  total: 0,
  Start: 100,
  FirstAttempt: 100,
  onAttempt: 100,
  minPossible: 25
};

inviteToPlay();

function game() {
  do {
    pickNewNumber();

    playerResponse = Number(chooseNumber());
    console.log('playerResponse: ' + playerResponse);

    if (isNaN(playerResponse)) {
      inviteContinueGame(null);
      if (continueGame === true) {
        continue;
      } else { 
        break; 
      }
    }

    successfulAttempt = playerResponse === number.winNumber;
    console.log('successfulAttempt: ' + successfulAttempt);
    getResultOf_Attempt(successfulAttempt);

    inviteContinueGame();
  } while (attempts.left > 0 && continueGame === true);
}

function inviteToPlay() {
  gameInvitation_accepted = confirm('Do you want to play a game?');

  if (!gameInvitation_accepted) {
    alert('You did not become a billionaire, but can.');
    return;
  } else {
    game();
  }
}

function pickNewNumber() {
  number.winNumber = Math.floor(Math.random() * (number.maxValue + 1));
  console.log(number.winNumber);
}

function chooseNumber() {
  let response;
  do {
    response =
      prompt(`Choose a roulette pocket number from 0 to ${number.maxValue}\n
    Attempts left: ${attempts.left}
    Total prize: ${prize.total}$ 
    Possible prize on current attempt: ${prize.onAttempt}$`);

    console.log('response: ' + response);

    if (
      response !== null &&
      String(response).trim() !== '' &&
      response >= 0 &&
      response <= number.maxValue &&
      Number.isInteger(Number(response))
    ) {
      console.log(response);
      return response;
    }

    if (response === null) {
      return 'NaN';
    }

    alert('Invalid input data');
  } while (infinitLoop);
}

function resetGame_forNewLevel() {
  if (!successfulAttempt && attempts.left <= 0) {
    attempts.left = attempts.maxValue;
    prize.onAttempt = prize.Start;
    prize.total = 0;
  }
}

function getResultOf_Attempt(successfulAttempt) {
  if (successfulAttempt) {
    prize.total += prize.onAttempt;
    prize.onAttempt = prize.FirstAttempt *= 2;
    number.maxValue += 4;
    attempts.left = attempts.maxValue;
  }

  if (!successfulAttempt) {
    --attempts.left;
    cutOff_Prize();

    if (attempts.left === 0) {
      prize.total = 0;
      prize.onAttempt = 0;
      number.maxValue = number.startValue;
    }
  }
}

function cutOff_Prize() {
  prize.onAttempt /= 2;
}

function inviteContinueGame(response = 0) {
  if (response === null) {
    continueGame = confirm(
      `Thank you for your participation. Your prize is: ${prize.total}$
                  Do you want to continue?`
    );
  } else {
    if (successfulAttempt) {
      continueGame = confirm(
        `Congratulation, you won!   Your prize is: ${prize.total}$. Do you want to continue?`
      );
      if (!continueGame) {
        continueGame = confirm(
          `Thank you for your participation. Your prize is: ${prize.total}$
                    Do you want to continue?`
        );
      }
    }

    if (!successfulAttempt) {
      continueGame = confirm(
        `Thank you for your participation. Your prize is: ${prize.total}$
                Do you want to continue?`
      );
    }
  }

  if (continueGame) {
    resetGame_forNewLevel();
  }
}