'use strict';

$('.input__item.number span').click(function () {
  calculator.addOperand($(this).text());
});

$('.input__item.clear span').click(function () {
  calculator.clearOperand();
});

$('.input__item.operator span').click(function () {
  calculator.addOperator($(this).text());
});

$('.input__item.result span').click(function () {
  calculator.getResult();
});

$('body').on('click', '.logs__circle span', function () {
  $(this).toggleClass('logs__circle--filled ');
});
$('body').on('click', '.logs__close span', function () {
  $(this).parents('.logs__item').remove();
});

$('.logs').scroll(function () {
  console.log('Scroll Top: ' + $(this).scrollTop());
});

////////////////////////////////////////////////////////////////////////////

class Operand {
  constructor() {
    this.positive = true;
    this.number;
  }

  convertNumber() {
    this.positive = !this.positive;
    this.number *= -1;
    return this.positive;
  }

  updateNumber(value) {
    this.number = value;
    console.log('current number: ' + value + '(' + typeof value + ')');
  }
}

class Calculator {
  constructor() {
    this.previousOperand = new Operand();
    this.currentOperand = new Operand();
    this.operator = '';
    this.state = 'firstOperand';

    this.text_mainOutput = '0';
    this.mainDisplay = document.getElementById('mainDisplay');
  }

  addOperand(textVal) {
    switch (this.state) {
      case 'firstOperand':
        if (this.operator !== '') {
          this.previousOperand = this.currentOperand;
          this.currentOperand = new Operand();
          this.state = 'secondOperand';
        }
        break;
      case 'postResult':
        this.previousOperand = this.currentOperand;
        this.currentOperand = new Operand();
        this.state = 'secondOperand';
        break;
      case 'result':
        this.currentOperand = new Operand();
        this.previousOperand = new Operand();
        this.operator = '';
        this.state = 'firstOperand';
        break;
      default:
    }

    if (this.currentOperand.number === undefined) {
      this.currentOperand.number = 0;
    }

    if (this.text_mainOutput === '0') {
      this.text_mainOutput = '';
    }
    if (textVal === '.' && this.text_mainOutput === '') {
      this.text_mainOutput = '0';
    }

    this.text_mainOutput += textVal;

    this.updateMainDisplay();
    this.currentOperand.updateNumber(+this.text_mainOutput);
  }

  deleteOperand() {
    if (this.text_mainOutput.length >= 2) {
      this.text_mainOutput = this.text_mainOutput.slice(0, -1);
    } else {
      this.text_mainOutput = '0';
    }
    this.updateMainDisplay();
    this.currentOperand.updateNumber(+this.text_mainOutput);
    this.history.hidePreviousCalculation();
  }

  addOperator(oper) {
    if (this.state === 'result') {
      this.currentOperand = this.previousOperand;
      this.state = 'firstOperand';
    }

    this.text_mainOutput = '';

    this.operator = oper;
  }

  getResult() {
    if (
      this.operator !== '' &&
      this.previousOperand.number !== undefined &&
      this.currentOperand.number !== undefined
    ) {
      let cop = this.currentOperand.number;
      let pop = this.previousOperand.number;
      let result;

      let color = '#000';

      switch (this.operator) {
        case '+':
          result = pop + cop;
          break;
        case '-':
          result = pop - cop;
          break;
        case '*':
          result = pop * cop;
          break;
        case '/':
          result = pop / cop;
          break;
        default:
      }

      if (!isFinite(result)) {
        console.log('error - divided by 0');
        result = 'ERROR';
        color = 'red';
      } else {
        this.postHistory(pop, cop, result, this.operator);
      }

      this.text_mainOutput = result;
      this.updateMainDisplay(color);

      this.state = 'result';
      this.previousOperand.number = result;
    }
  }

  clearOperand() {
    let cop = this.currentOperand;
    let pop = this.previousOperand;

    if (this.state === 'result') {
      pop.number = 0;
      this.previousOperand = new Operand();
      this.currentOperand = new Operand();
      this.operator = '';
      this.state = 'firstOperand';
    }

    if (cop.number === 0) {
      pop.number = 0;
      this.previousOperand = new Operand();
      this.currentOperand = new Operand();
      this.operator = '';
      this.state = 'firstOperand';
    } else {
      cop.number = 0;
    }

    this.text_mainOutput = cop.number;

    this.updateMainDisplay();

    console.log(
      'expression: ' +
        `${this.previousOperand.number} ${this.operator} ${this.currentOperand.number}`
    );
  }

  convertSign() {
    this.currentOperand.convertNumber();
    this.text_mainOutput = this.currentOperand.number;
    this.updateMainDisplay();
    console.log(
      'convert number| current operand: ' + this.currentOperand.number
    );
  }

  getPercent() {
    this.currentOperand.number /= 100;

    this.text_mainOutput = this.currentOperand.number;
    this.currentOperand.updateNumber(+this.text_mainOutput);
    this.updateMainDisplay();
  }

  updateMainDisplay(color = '#000') {
    this.mainDisplay.innerHTML = this.text_mainOutput;
    $('#mainDisplay').css({ color: color });
  }

  postHistory(oper1, oper2, result, symbol) {
    if (oper1 && oper2) {
      let equation = `${oper1} ${symbol} ${oper2} = ${result}`;

      let span = '<span>';
      if (equation.includes('48')) {
        span = "<span style='text-decoration: underline'>";
      }

      let item = `
          <div class="logs__item">
        <div class="logs__circle"><span></span></div>
        <div class="logs__equation">${span}${equation}</span></div>
        <div class="logs__close"><span>X</span></div>
      </div>`;

      $('.logs').prepend(item);
    }
  }
}

////////////////////////////////////////////////////////////////////////////

let calculator = new Calculator();
let historyOutput = document.getElementById('history_output');
let historyHidden = true;