'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 * function PizzaException(...) { ... } 
 */
function Pizza(size, type) {
  if (!new.target) {
    return new Pizza(size, type);
  }

  if (!size || !type || arguments.length !== 2) {
    throw new PizzaException(
      `Required two arguments, given: ${arguments.length}`
    );
  }

  if (!Pizza.allowedSizes.includes(size)) {
    throw new PizzaException(`Invalid size`);
  }
  if (!Pizza.allowedTypes.includes(type)) {
    throw new PizzaException(`Invalid type`);
  }

  this.size = size;
  this.type = type;

  this.extraIngredients = [];

  this.addExtraIngredient = function (ingredient) {
    if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
      throw new PizzaException(`Invalid ingredient`);
    }
    if (this.extraIngredients.includes(ingredient)) {
      throw new PizzaException('Duplicate ingredient');
    }

    this.extraIngredients.push(ingredient);
  };
  this.removeExtraIngredient = function (ingredient) {
    for (let i = 0; i < this.extraIngredients.length; i++) {
      if (this.extraIngredients[i] === ingredient) {
        this.extraIngredients.splice(i, 1);
      }
    }
  };

  this.getExtraIngredients = function () {
    return this.extraIngredients;
  };
  this.getSize = function () {
    return this.size;
  };
  this.getPrice = function () {
    let price = this.size.price + this.type.price;
    this.extraIngredients.forEach((element) => {
      price += element.price;
    });
    return price;
  };
  this.getPizzaInfo = function () {
    return `Size: ${this.size.size}, type: ${
      this.type.type
    }; extra ingredients: ${this.extraIngredients.map(
      (item) => item.ingredient
    )}; price: ${this.getPrice()}.`;
  };
}

/* Class example */
// class Pizza {
//   constructor(size, type) {
//     if (!size || !type || arguments.length !== 2) {
//       throw new PizzaException(
//         `Required two arguments, given: ${arguments.length}`
//       );
//     }

//     if (!Pizza.allowedSizes.includes(size)) {
//       throw new PizzaException(`Invalid size`);
//     }
//     if (!Pizza.allowedTypes.includes(type)) {
//       throw new PizzaException(`Invalid type`);
//     }

//     this.size = size;
//     this.type = type;

//     this.extraIngredients = [];
//   }

//   addExtraIngredient = function (ingredient) {
//     if (!Pizza.allowedExtraIngredients.includes(ingredient)) {
//       throw new PizzaException(`Invalid ingredient`);
//     }
//     if (this.extraIngredients.includes(ingredient)) {
//       throw new PizzaException("Duplicate ingredient");
//     }
// }


/* Sizes, types and extra ingredients */
Pizza.SIZE_S = { size: 'SMALL', price: 50 };
Pizza.SIZE_M = { size: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { size: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { type: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { type: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { type: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { ingredient: 'TOMATOES', price: 5 };
Pizza.EXTRA_CHEESE = { ingredient: 'CHEESE', price: 7 };
Pizza.EXTRA_MEAT = { ingredient: 'MEAT', price: 9 };

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [
  Pizza.TYPE_VEGGIE,
  Pizza.TYPE_MARGHERITA,
  Pizza.TYPE_PEPPERONI
];
Pizza.allowedExtraIngredients = [
  Pizza.EXTRA_TOMATOES,
  Pizza.EXTRA_CHEESE,
  Pizza.EXTRA_MEAT
];

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 * function PizzaException(...) { ... }
 */

class PizzaException extends Error {
  constructor(message) {
    super(message);
    this.name = 'PizzaException';
  }
}

/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_SPONGE); // => Invalid ingredient